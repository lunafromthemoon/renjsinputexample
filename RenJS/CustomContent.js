RenJS.customContent = {
	//put here your own functions

	helloworld: function (params) {
		console.log("helloworld function");
		console.log(params.param1 + " " +params.param2);
		RenJS.resolve();
	},

	showLink: function(params) {
		//create the button
		var button = game.add.button(480,330,"linkbutton",function(){
			window.open(params.link,'_blank');
		},this,1,0,1,0);
		//story it in customContent so we can delete it later
		RenJS.customContent.linkButton = button;
		//create the button text
		var text = game.add.text(0,0, params.title, 
			{font: "20pt audimat-mono", boundsAlignH: "center", boundsAlignV: "middle"});
		//set the text boundaries so it will be centered in the button
		text.setTextBounds(0,0, button.width,button.height);
		//add text to the button
		button.addChild(text);
		//Continue with the story
		RenJS.resolve();
	},

	hideLink: function(params) {
		//Destroy the button
		RenJS.customContent.linkButton.destroy();
		//Continue with the story
		RenJS.resolve();
	},

	showInput: function(){
		//Create the input
		var input = game.add.inputField(10, 90, {
	    font: '20px audimat-mono',
	    fill: '#212121',
	    fontWeight: 'bold',
	    width: 150,
	    padding: 8,
	    borderWidth: 1,
	    borderColor: '#000',
	    borderRadius: 6,
	    placeHolder: 'Name'
		});
		RenJS.customContent.nameInput = input;
		input.startFocus();﻿﻿
		//Capture event for enter
   	input.domElement.element.addEventListener('keydown', (function(event){
   		if (event.keyCode === 13) { //If event is enter
    	  console.log("The input name is "+RenJS.customContent.nameInput.value);
    	  // Save it as a game variable (username) to use it on the story text
    	  RenJS.logicManager.vars.username = RenJS.customContent.nameInput.value;
    	  // Destroy the input
				RenJS.customContent.nameInput.destroy();
				// Continue with the story
				RenJS.resolve();
    	}
   	}).bind(game));﻿﻿
	},

}

